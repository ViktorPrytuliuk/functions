const getSum = (str1, str2) => {
  if (isNaN(Number(str1)) || typeof str1 !== 'string') {
    return false
  }
  if (isNaN(Number(str2)) || typeof str2 !== 'string') {
    return false
  }
  return (BigInt(str1) + BigInt(str2)).toString()
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;

  listOfPosts.forEach(el => {
    if (el.author === authorName) {
      posts++
    }
    if (el.comments) {
      el.comments.forEach(comment => {
        if (comment.author === authorName) {
          comments++
        }
      })
    }
  });
  return `Post:${posts},comments:${comments}`;
};

const tickets = queue => {
  let cashbox25 = 0;
  let cashbox50 = 0;
  let cashbox100 = 0;
  queue.forEach(bill => Number(bill)); 

  for (let bill of queue) {
      if (bill === 25) {
        cashbox25++;
      }

      if (bill === 50) {
          if (cashbox25) {
            cashbox50++;
            cashbox25--;
          } else {
              return 'NO'
          } 
      }

      if (bill === 100) {
          if (cashbox25 >= 1 && cashbox50 >= 1) {
            cashbox100++;
            cashbox25--;
            cashbox50--; 
          } else if(cashbox25 >= 3) {
            cashbox100++;
            cashbox25-=3;
          }  else {
              return 'NO'
          }
      }
  }
  return 'YES'
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};